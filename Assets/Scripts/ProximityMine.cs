﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityMine : MonoBehaviour
{
    private bool mineActive;
    public GameObject owner;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("I found you");
        if (other.GetComponent<PlayerController>() != null && other.gameObject != owner)
        {
            Debug.Log("I have you now skywalker");
            if (other.GetComponent<PlayerController>().active)
            {
                other.GetComponent<PlayerController>().Death();
            }
            //Extremely hacky fix for current , needs re do
            //if the hit player instance is the current character, regardless of active, it needs to die
            else if (GameObject.Find("GameManager").GetComponent<GameManager>().currentCharacter == other)
            {
                other.GetComponent<PlayerController>().enabled = true;
                other.GetComponent<PlayerController>().Death();
                other.GetComponent<PlayerController>().enabled = false;
            }
        }
    }
}
