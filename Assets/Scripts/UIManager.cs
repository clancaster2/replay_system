﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameManager manager;
    public GameObject newRoundUI;
    public Text newRoundText;
    public GameObject GameOver;
    public Text GameOverText;
	public GameObject pauseMenu;

	public delegate void SaveLoad();
	public static event SaveLoad GameSaved;

    // Use this for initialization
    void Start()
    {
        GameManager.HideRoundUI += HideRoundButton;
        GameManager.ShowRoundUI += ShowRoundButton;
        GameManager.ShowEndUI += ShowGOUI;
        RecordingManager.replayFinished += ShowRoundButton;
		RecordingManager.paused += ShowPause;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ShowRoundButton()
    {
        if (manager.activeTeam == GameManager.teams.red)
        {
            newRoundText.color = Color.blue;
            newRoundText.text = "It is now blue teams turn";
        }
        else
        {
            newRoundText.color = Color.red;
            newRoundText.text = "It is now red teams turn";
        }
        newRoundUI.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    void HideRoundButton()
    {
        newRoundUI.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

    void ShowGOUI(GameManager.teams victor)
    {
        if(newRoundUI.activeInHierarchy)
        {
            HideRoundButton();
        }

        GameOver.gameObject.SetActive(true);
        GameOverText.text = "A weiner is " + victor;
        if (victor == GameManager.teams.blue)
        {
            GameOverText.color = Color.blue;
        }
        else if (victor == GameManager.teams.red)
        {
            GameOverText.color = Color.red;
        }
        Cursor.lockState = CursorLockMode.None;
    }

	private void ShowPause()
	{
		pauseMenu.SetActive(true);
		Cursor.lockState = CursorLockMode.None;
	}

	public void Save()
	{
		if (GameSaved != null) GameSaved();
		SaveManager.saveManager.Save();
		pauseMenu.SetActive(false);
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void Load()
	{
		SaveManager.saveManager.Load();
		pauseMenu.SetActive(false);
		Cursor.lockState = CursorLockMode.Locked;
	}

    private void OnDisable()
    {
        GameManager.HideRoundUI -= HideRoundButton;
        GameManager.ShowRoundUI -= ShowRoundButton;
        GameManager.ShowEndUI -= ShowGOUI;
        RecordingManager.replayFinished -= ShowRoundButton;
		RecordingManager.paused -= ShowPause;
	}
}
