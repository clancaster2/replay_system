﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class PlayerController : RecordableObject
{
    public float speed = 5.0f;
    public float jumpSpeed = 150.0f;
    public KeyCode jumpKey;
    public float distToGround;

    public GameManager.teams myTeam;

    public bool active;

    public float roundTimeLimit;
    private float roundTime;

    public Vector3 endLoc;
    public Vector3 startLoc;

    public delegate void PlayerInput();
    public event PlayerInput Fire;
    public delegate void PlayerDead();
    public static event PlayerDead Dead;

    public GameObject minePrefab;
    public int minesHeld = 1;

    // Use this for initialization
    void Start()
    {
        //Disables the mouse cursor
        Cursor.lockState = CursorLockMode.Locked;
        //Sets the distance to ground variable using the players height
        distToGround = this.GetComponent<Collider>().bounds.extents.y;
        startLoc = transform.position;
        recorder.AddObject(gameObject);
        active = true;
        roundTimeLimit = 0;
		KeyframeTransform initialTransKey = new KeyframeTransform();
		initialTransKey.posX = transform.position.x;
		initialTransKey.posY = transform.position.y;
		initialTransKey.posZ = transform.position.z;
		initialTransKey.rotX = transform.rotation.x;
		initialTransKey.rotY = transform.rotation.y;
		initialTransKey.rotZ = transform.rotation.z;
		Record(initialTransKey);
	}

    // Update is called once per frame
    void Update()
    {
        timestamp += Time.deltaTime;
        recordTimer += Time.deltaTime;
        roundTime += Time.deltaTime;

        //Creates an absolute transform keyframe every second
        if (recordTimer > 1)
        {
            recordTimer = 0;
            KeyframeTransform absTransKey = new KeyframeTransform();
			absTransKey.posX = transform.position.x;
			absTransKey.posY = transform.position.y;
			absTransKey.posZ = transform.position.z;
			absTransKey.rotX = transform.eulerAngles.x;
			absTransKey.rotY = transform.eulerAngles.y;
			absTransKey.rotZ = transform.eulerAngles.z;
			Record(absTransKey);
        }

		if (Input.GetMouseButtonDown(0))
		{
			//Only fires when the cursor is locked, meaning it shouldn't fire when using UI
			if (Cursor.lockState == CursorLockMode.Locked)
			{
				PlayerFire();
			}
		}

        //Fires and ends the round if player does not fire by the round time limit
        if (GameObject.Find("GameManager").GetComponent<GameManager>().roundTimer <= roundTimeLimit & active)
        {
            Debug.Log("Force Fire");
            PlayerFire();
        }

        //Records a movement keyframe when any WASD key is held
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            KeyframeTransform transKey = new KeyframeTransform();
			transKey.posX = transform.position.x;
			transKey.posY = transform.position.y;
			transKey.posZ = transform.position.z;
			transKey.rotX = transform.eulerAngles.x;
			transKey.rotY = transform.eulerAngles.y;
			transKey.rotZ = transform.eulerAngles.z;
			Record(transKey);
        }

        //Interaction Input Check (Currently for toggling lights
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 100))
            {
                if (hit.collider.gameObject.GetComponentInParent<LightSwitch>() != null)
                {
                    hit.collider.gameObject.GetComponentInParent<LightSwitch>().ToggleLight();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            PlaceMine();
        }
    }
    public void PlayerFire()
    {
        //Records a transform keyframe when firing to be as accurate as possible
        KeyframeTransform transKey = new KeyframeTransform();
		transKey.posX = transform.position.x;
		transKey.posY = transform.position.y;
		transKey.posZ = transform.position.z;
		transKey.rotX = transform.eulerAngles.x;
		transKey.rotY = transform.eulerAngles.y;
		transKey.rotZ = transform.eulerAngles.z;
		Record(transKey);
        //Records the fire keyframe
        KeyframeFire fireKey = new KeyframeFire();
        Record(fireKey);
        Fire();
        active = false;
    }

    public void ReplayFire()
    {
        if (GameObject.Find("GameManager").GetComponent<GameManager>().currentCharacter != this.gameObject)
        {
            if (Fire != null) Fire();
        }
    }

    private void PlaceMine()
    {
        if (minesHeld > 0)
        {
            ProximityMine newMine = Instantiate(minePrefab, this.transform.position, Quaternion.identity).GetComponent<ProximityMine>();
            newMine.owner = this.gameObject;
            minesHeld -= 1;
        }
    }

    void FixedUpdate()
    {
        //Basic WASD movement, inputs are pre existing in Unity
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        //transform.Rotate(0, x, 0);
        transform.Translate(x, 0, z);

        if (Input.GetKeyDown("space") && IsGrounded())
        {
            //Gives the player upward velocity to act as a jump
            Jump();
            KeyframeJump jumpKeyframe = new KeyframeJump();
            Record(jumpKeyframe);
        }

        if (IsGrounded())
        {
            speed = 5.0f;
        }
        else
        {
            //reduces the players speed if they are in the air to remove some control
            speed = 3.0f;
        }
    }

    public void Jump()
    {
        this.GetComponent<Rigidbody>().velocity = transform.up * Time.deltaTime * jumpSpeed;
    }

    bool IsGrounded()
    {
        //Checks if the player is in contact with the ground
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    public void Death()
    {
		active = false;
        if (Dead != null) Dead();
    }
}
