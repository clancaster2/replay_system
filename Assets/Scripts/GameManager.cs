﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public GameObject characterPrefab;
	public GameObject currentCharacter;
	public Vector3 spawnRot;
	public Vector3 spawnLoc;
	[SerializeField]
	private RecordingManager recordingManager;

	public delegate void ToggleRoundUI();
	public static event ToggleRoundUI ShowRoundUI;
	public static event ToggleRoundUI HideRoundUI;
	public delegate void MatchEnd(teams victor);
	public static event MatchEnd ShowEndUI;

	public enum teams { red, blue };
	public teams activeTeam;
	public teams matchVictor;

	[HideInInspector]
	public bool isFirstRound;
	[HideInInspector]
	public bool isGameActive;
	[HideInInspector]
	public int roundCounter = 0;

	public Material playerMatRed;
	public Material playerMatBlue;
	[HideInInspector]
	public Material currentPlayerMat;

	public float roundTimer;
	public float hiddenRoundTimer;
	public Text timerText;

	private void Awake()
	{
		currentCharacter.GetComponent<PlayerController>().recorder = recordingManager;
	}

	// Use this for initialization
	void Start()
	{
		WeaponFire.Fired += endRound;
		PlayerController.Dead += EndGame;
		isGameActive = true;
		isFirstRound = true;
		activeTeam = teams.red;
		roundCounter += 1;
		currentPlayerMat = playerMatRed;
		currentCharacter.GetComponent<Renderer>().material = currentPlayerMat;
	}

	// Update is called once per frame
	void Update()
	{
		if (roundTimer > 0)
		{
			roundTimer -= Time.deltaTime;
		}
		else
		{
			roundTimer = 0;
		}
		roundTimer = Mathf.Round(roundTimer * 100) / 100;
		timerText.text = "Time: " + roundTimer;

		hiddenRoundTimer += Time.deltaTime;
	}

	//Disables the current character, sets it to its original location, and spawns a new character.
	public void endRound()
	{
		if (isGameActive)
		{
			//Store information needed to spawn next character.
			spawnLoc = currentCharacter.transform.position;
			spawnRot = new Vector3(currentCharacter.transform.eulerAngles.x, currentCharacter.transform.eulerAngles.y, currentCharacter.transform.eulerAngles.z);
			////Store character within the recorder
			//recordingManager.AddObject(currentCharacter);
			//Disable Character
			currentCharacter.GetComponent<PlayerController>().enabled = false;
			Destroy(currentCharacter.GetComponentInChildren<FlareLayer>());
			currentCharacter.GetComponentInChildren<CamMouseLook>().enabled = false;
			currentCharacter.GetComponentInChildren<AudioListener>().enabled = false;
			//Enables the endround UI on the first round
			if (isFirstRound)
			{
				isFirstRound = false;
				if (ShowRoundUI != null) ShowRoundUI();
			}
		}
	}

	public void StartRound()
	{
		if (isGameActive)
		{
			//Changes the active team/player
			if (activeTeam == teams.red)
			{
				activeTeam = teams.blue;
				currentPlayerMat = playerMatBlue;
			}
			else
			{
				activeTeam = teams.red;
				currentPlayerMat = playerMatRed;
			}
			print("It is now " + activeTeam + "'s turn to go");
			if (HideRoundUI != null) HideRoundUI();
			if (currentCharacter != null)
			{
				currentCharacter.transform.position = currentCharacter.GetComponent<PlayerController>().startLoc;
				Destroy(currentCharacter.GetComponentInChildren<Camera>());
			}
			//Spawn next character, assigning the correct rotation and position, along with assigning the teams material and the recording manager.
			currentCharacter = Instantiate(characterPrefab, spawnLoc, Quaternion.identity);
			currentCharacter.GetComponent<Renderer>().material = currentPlayerMat;
			currentCharacter.GetComponentInChildren<CamMouseLook>().mouseLook.x = spawnRot.y;
			currentCharacter.GetComponent<PlayerController>().recorder = recordingManager;
            currentCharacter.GetComponent<PlayerController>().myTeam = activeTeam;

            if (currentCharacter.GetComponent<PlayerController>().Keyframes.Count > 0)
            {
                Debug.Log("Yo WTF, there's " + currentCharacter.GetComponent<PlayerController>().Keyframes.Count + " keyframes here!");
            }

            roundCounter += 1;
			roundTimer = 10;
			hiddenRoundTimer = 0;
			//Starts the replay function
			recordingManager.PlayRecording();
		}
	}

	public void EndGame()
	{
		print(roundCounter + " rounds survived");
		isGameActive = false;
		//The active player must have died, therefore the non active player is the weiner
		switch (activeTeam)
		{
			case teams.blue:
				print("A weiner is red");
				matchVictor = teams.red;
				break;
			case teams.red:
				print("A weiner is blue");
				matchVictor = teams.blue;
				break;
		}
		currentCharacter.GetComponent<PlayerController>().enabled = false;
		currentCharacter.GetComponentInChildren<CamMouseLook>().enabled = false;
		Cursor.lockState = CursorLockMode.None;
		ShowEndUI(matchVictor);
	}

	public void RestartGame()
	{
		//Restart the scene here 
		//print("The game would restart here if the dev wasn't a lazy asshole");
		SceneManager.LoadScene("TestScene");
	}

	private void OnDisable()
	{
		WeaponFire.Fired -= endRound;
		PlayerController.Dead -= EndGame;
	}
}
