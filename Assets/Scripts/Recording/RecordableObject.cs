﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class RecordableObject : MonoBehaviour
{
    public RecordingManager recorder;
    public float recordTimer;
    public float timestamp = 0;
    public List<KeyframeBase> Keyframes = new List<KeyframeBase>();
    public List<KeyframeTransform> transformKey = new List<KeyframeTransform>();
    public List<KeyframeJump> jumpKeyframe = new List<KeyframeJump>();
    public delegate void Recording();
    public static event Recording finishedReplaying;

    public float tempTimeStamp;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Record(KeyframeBase newKeyframe)
    {
        newKeyframe.timestamp = timestamp;
        if (newKeyframe is KeyframeFire)
        {
            Keyframes.Add(newKeyframe as KeyframeFire);
        }
        else if (newKeyframe is KeyframeTransform)
        {
            Keyframes.Add(newKeyframe as KeyframeTransform);
        }
        else if (newKeyframe is KeyframeJump)
        {
            Keyframes.Add(newKeyframe as KeyframeJump);
        }
        else if (newKeyframe is KeyframeLightToggle)
        {
            Keyframes.Add(newKeyframe as KeyframeLightToggle);
        }
    }

    public IEnumerator Replay()
    {
        {
            if (Keyframes.Count > 0 && this.gameObject != GameObject.Find("GameManager").GetComponent<GameManager>().currentCharacter)
            {
                foreach (KeyframeBase key in Keyframes)
                {
                    yield return new WaitForSeconds(key.timestamp - GameObject.Find("GameManager").GetComponent<GameManager>().hiddenRoundTimer);
                    if (key is KeyframeFire)
                    {
                        gameObject.GetComponent<PlayerController>().ReplayFire();
                    }
                    else if (key is KeyframeTransform)
                    {
                        KeyframeTransform newTransKey = key as KeyframeTransform;
                        transform.position = new Vector3(newTransKey.posX, newTransKey.posY, newTransKey.posZ);
                        transform.rotation = Quaternion.Euler(new Vector3(newTransKey.rotX, newTransKey.rotY, newTransKey.rotZ));
                    }
                    else if (key is KeyframeJump)
                    {
                        Debug.Log("Jump");
                        gameObject.GetComponent<PlayerController>().Jump();
                    }
                    else if (key is KeyframeLightToggle)
                    {
                        gameObject.GetComponent<LightSwitch>().ReplayToggle();
                    }
                    tempTimeStamp = key.timestamp;
                }
                if (finishedReplaying != null) finishedReplaying();
            }
        }
    }
}

[System.Serializable]
public class KeyframeBase
{
    public float timestamp;
}

[System.Serializable]
public class KeyframeFire : KeyframeBase
{

}

//Basic keyframe that stores the objects transform at specific timeframes
[System.Serializable]
public class KeyframeTransform : KeyframeBase
{
    public float posX;
    public float posY;
    public float posZ;
    public float rotX;
    public float rotY;
    public float rotZ;
}

[System.Serializable]
public class KeyframeJump : KeyframeBase
{

}

[System.Serializable]
public class KeyframeLightToggle : KeyframeBase
{

}