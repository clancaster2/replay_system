﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class RecordingManager : MonoBehaviour
{
    public List<GameObject> RecordedObjects = new List<GameObject>();

    public bool isReplaying;
    public bool playerDone;

    public int numObjReplayFinished;

    public delegate void Recording();
    public static event Recording replayFinished;
    public static event Recording paused;

    private List<Coroutine> replays = new List<Coroutine>();

    // Use this for initialization
    void Start()
    {
        RecordableObject.finishedReplaying += IncrementFinished;
        WeaponFire.Fired += PlayerFired;
        UIManager.GameSaved += OnSave;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            CancelReplay();
        }
    }

    //Adds given recordableobject to list of objects being recorded
    public void AddObject(GameObject objectToAdd)
    {
        RecordedObjects.Add(objectToAdd);
    }

    public void PlayRecording()
    {
        isReplaying = true;
        foreach (GameObject item in RecordedObjects)
        {
            //CancelRecording();
            if (item.GetComponent<PlayerController>() != null)
            {
                item.GetComponent<PlayerController>().timestamp = 0;
                item.GetComponentInChildren<WeaponFire>().lineRend.enabled = false;
                replays.Add(StartCoroutine(item.GetComponent<PlayerController>().Replay()));
            }
            else if (item.GetComponent<LightSwitch>() != null)
            {
                item.GetComponent<LightSwitch>().timestamp = 0;
                StartCoroutine(item.GetComponent<LightSwitch>().Replay());
            }
        }
    }

    private void CancelReplay()
    {
        //If there are any replays, stop them all
        if (replays.Count > 0)
        {
            foreach (Coroutine replayCoroutine in replays)
            {
                StopCoroutine(replayCoroutine);
            }
        }
        if (paused != null) paused();
    }

    private void IncrementFinished()
    {
        numObjReplayFinished++;
        FinishRound();
    }

    private void PlayerFired()
    {
        playerDone = true;
        FinishRound();
    }

    private void FinishRound()
    {
        if (CheckFinished() & playerDone)
        {
            //End the round here
            //Ensures gameobject exists
            if (GameObject.Find("GameManager") != null)
            {
                if (!GameObject.Find("GameManager").GetComponent<GameManager>().isFirstRound)
                {
                    if (GameObject.Find("GameManager").GetComponent<GameManager>().isGameActive)
                    {
                        if (replayFinished != null)
                            replayFinished();
                    }
                }
            }
            //Resets variables to prepare for next round
            replays.Clear();
            playerDone = false;
            isReplaying = false;
            numObjReplayFinished = 0;
        }
    }

    private bool CheckFinished()
    {
        //Checks if all items in recorded items have finished and returns the bool accordingly (- 1, as it does not include the current player, who is still in this list).
        if (numObjReplayFinished == RecordedObjects.Count - 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void OnSave()
    {
        foreach (GameObject obj in RecordedObjects)
        {
            RecordedData data = new RecordedData();
            foreach (KeyframeBase key in obj.GetComponent<RecordableObject>().Keyframes)
            {
                data.savedKeys.Add(key);
            }
            if (obj.GetComponent<PlayerController>() != null)
            {
                data.savedTeam = obj.GetComponent<PlayerController>().myTeam;
            }
            SaveManager.saveManager.records.Add(data);
        }
    }

    private void OnDisable()
    {
        RecordableObject.finishedReplaying -= IncrementFinished;
        WeaponFire.Fired -= PlayerFired;
        UIManager.GameSaved -= OnSave;
    }
}
