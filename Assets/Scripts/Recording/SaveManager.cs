﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
	public static SaveManager saveManager;

	[HideInInspector]
	public List<RecordedData> records = new List<RecordedData>();
	public List<RecordedData> loadedRecords = new List<RecordedData>();
	public GameObject InstantiationPrefab;
	public RecordingManager recordingManager;
	public GameManager gameManager;

    public delegate void LoadState();
    public static event LoadState charactersLoaded;

	void Awake()
	{
		//checks if a SaveManager already exists and if it doesnt, make this it
		if (saveManager == null)
		{
			//ensures whatever object this script is attached to is not destroyed when scenes load/unload
			DontDestroyOnLoad(gameObject);
			saveManager = this;
		}
		//if a SaveManager does exist and its not this, delete this
		else if (saveManager != this)
		{
			Destroy(gameObject);
		}
	}

	public void Save()
	{
		//Creates a file to write data to
		BinaryFormatter bf = new BinaryFormatter();
		//Currently just creates/overwrites the file every save, could be set up to check for file.exists then open file to write to
		FileStream file = File.Create(Application.persistentDataPath + "/ReplayInfo.dat");

		GameData data = new GameData();

		data.records = records;
		data.currentRound = gameManager.roundCounter;
		data.currentTeam = gameManager.activeTeam;
        data.StartPosX = gameManager.spawnLoc.x;
        data.StartPosY = gameManager.spawnLoc.y;
        data.StartPosZ = gameManager.spawnLoc.z;

        // Writes the information in data class to binary file created above, here is where the magic happens
        bf.Serialize(file, data);
        file.Close();

		Debug.Log("Saved " + data.records.Count);

		//Gonna put this here cause it might help maybe
		//Stops from bunch of overlaps or repeats
		records.Clear();
	}

	public void Load()
	{
		if (File.Exists(Application.persistentDataPath + "/ReplayInfo.dat"))
		{
			//Opens the data file and pulls the data/information in it to 'data'
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/ReplayInfo.dat", FileMode.Open);
			GameData data = (GameData)bf.Deserialize(file);
			file.Close();

			loadedRecords = data.records;
			gameManager.activeTeam = data.currentTeam;
			gameManager.roundCounter = data.currentRound;
            gameManager.spawnLoc = new Vector3(data.StartPosX, data.StartPosY, data.StartPosZ);

			ResetScene();
		}
	}

	private void ResetScene()
	{
		//As always this works, but find by name is very specific and not great for reusability
		foreach (GameObject obj in recordingManager.RecordedObjects)
		{
			//Deletes all the existing objects from the game
			Destroy(obj);
		}
		recordingManager.RecordedObjects.Clear();
		Destroy(gameManager.currentCharacter);

		//Creates a new playerobj instantiation for each on within the loaded data, filling in their keyframes and team variables
		foreach (RecordedData newData in loadedRecords)
		{ 
			PlayerController newObj = Instantiate(InstantiationPrefab).GetComponent<PlayerController>();
			newObj.Keyframes = newData.savedKeys;
            newObj.myTeam = newData.savedTeam;
            if (newObj.myTeam == GameManager.teams.red)
            {
                newObj.GetComponent<Renderer>().material = gameManager.playerMatRed;
            }
            else
            {
                newObj.GetComponent<Renderer>().material = gameManager.playerMatBlue;
            }
            foreach (KeyframeBase keys in newData.savedKeys)
			{
				if (keys is KeyframeTransform)
				{
					KeyframeTransform newTransKey = keys as KeyframeTransform;
					newObj.transform.position = new Vector3(newTransKey.posX, newTransKey.posY, newTransKey.posZ);
					newObj.recorder = recordingManager;
					newObj.GetComponent<PlayerController>().enabled = false;
					Destroy(newObj.GetComponentInChildren<FlareLayer>());
					newObj.GetComponentInChildren<CamMouseLook>().enabled = false;
					newObj.GetComponentInChildren<AudioListener>().enabled = false;
                    newObj.GetComponentInChildren<WeaponFire>().hasFired = true;
                    newObj.GetComponentInChildren<WeaponFire>().lineRend = newObj.GetComponentInChildren<LineRenderer>();
                    Destroy(newObj.GetComponentInChildren<Camera>());
					recordingManager.RecordedObjects.Add(newObj.gameObject);
					break;
				}
			}
		}
        //Once all the saved data has been loaded, start a new round as if continuing on the saved game
		gameManager.StartRound();
	}
}

//Container class that simply stores all the data that needs to be written to file 
//Proper use of [Serializable] to allow data to be serialized and wriiten to file 
[Serializable]
class GameData
{
	public List<RecordedData> records = new List<RecordedData>();
	public GameManager.teams currentTeam;
 	public int currentRound;
    public float StartPosX;
    public float StartPosY;
    public float StartPosZ;
}

