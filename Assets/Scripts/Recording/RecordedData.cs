﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RecordedData
{
	public List<KeyframeBase> savedKeys = new List<KeyframeBase>();
    public GameManager.teams savedTeam;
}
