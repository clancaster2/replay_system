﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFire : MonoBehaviour
{
    public bool hasFired;
    private RaycastHit hit;
    public GameObject spawn;
    public PlayerController myController;
    public LineRenderer lineRend;

    public delegate void weaponFired();
    public static event weaponFired Fired;

    // Use this for initialization
    void Start()
    {
        lineRend = GetComponent<LineRenderer>();
        lineRend.enabled = false;
        myController = transform.root.GetComponent<PlayerController>();
        myController.Fire += Fire;
        SaveManager.charactersLoaded += OnLoad;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnLoad()
    {
        hasFired = true;
    }

    private void Fire()
    {
        //Fires a raycast out from the weapon to create a hitscan bullet mechanic
        if (Physics.Raycast(spawn.transform.position, spawn.transform.forward, out hit, 1000))
        {
            //Draws the above ray
            Debug.DrawRay(spawn.transform.position, spawn.transform.forward * hit.distance, Color.red, 0.5f);
            lineRend.enabled = true;
            lineRend.positionCount = 2;
            lineRend.SetPosition(0, this.gameObject.transform.position);
            lineRend.SetPosition(1, hit.point);
            //Tells the script that the player has now fired their weapon
            if (!hasFired)
            { 
                hasFired = true;
                //Gets the final location of the player object and ends the round
                if (Fired != null) Fired();
            }

            CheckPlayerHit(hit.collider.gameObject);
            
            //If the raycast hits a mirror, fire a new raycast to simulate a reflection
            if (hit.collider.gameObject.GetComponent<Mirror>() != null)
            {
                mirrorReflection(spawn.transform.forward, hit.normal, hit.point);
            }
        }
    }

    private void mirrorReflection(Vector3 inDir, Vector3 inNorm, Vector3 hitPoint)
    {
        //Works out the direction of the reflection
        Vector3 reflectDir;
        reflectDir = Vector3.Reflect(inDir, inNorm);
        RaycastHit reflectHit;
        if (Physics.Raycast(hitPoint, reflectDir, out reflectHit, 1000))
        {
            Debug.DrawRay(hitPoint, reflectDir * reflectHit.distance, Color.green, 0.5f);
            CheckPlayerHit(reflectHit.collider.gameObject);
            lineRend.positionCount = 3;
            lineRend.SetPosition(2, reflectHit.point);
        }
    }

    private void CheckPlayerHit(GameObject obj)
    {
        if (obj.GetComponent<PlayerController>() != null)
        {
            if (obj.GetComponent<PlayerController>().active)
            {
                obj.GetComponent<PlayerController>().Death();
            }
            //Extremely hacky fix for current , needs re do
            //if the hit player instance is the current character, regardless of active, it needs to die
            else if (GameObject.Find("GameManager").GetComponent<GameManager>().currentCharacter == obj)
            {
                obj.GetComponent<PlayerController>().enabled = true;
                obj.GetComponent<PlayerController>().Death();
                obj.GetComponent<PlayerController>().enabled = false;
            }
        }
    }

    private void OnDisable()
    {
        SaveManager.charactersLoaded -= OnLoad;
        myController.Fire -= Fire;
    }
}

