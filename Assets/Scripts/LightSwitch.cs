﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : RecordableObject
{
    public Light targetLight;
    public bool isOn;

    // Use this for initialization
    void Start()
    {
        if (targetLight.isActiveAndEnabled)
        {
            isOn = true;
        }
        else
        {
            isOn = false;
        }
        recorder.AddObject(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        timestamp += Time.deltaTime;
    }

    public void ToggleLight()
    {
        if (isOn)
        {
            //Turns off the light
            targetLight.enabled = false;
        }
        else if (!isOn)
        {
            //Turns on the light
            targetLight.enabled = true;
        }
        //toggles the bool representing the on state of the light
        isOn = !isOn;
        //Creates and records a new keyframe for the toggling of the light
        KeyframeLightToggle lightKey = new KeyframeLightToggle();
        Record(lightKey);
    }

    public void ReplayToggle()
    {
        if (isOn)
        {
            //Turns off the light
            targetLight.enabled = false;
        }
        else if (!isOn)
        {
            //Turns on the light
            targetLight.enabled = true;
        }
        //toggles the bool representing the on state of the light
        isOn = !isOn;
    }
}
