﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMouseLook : MonoBehaviour
{
	public Vector2 mouseLook;
	private Vector2 smoothV;
	public float sensitivity = 5.0f;
	public float smoothing = 2.0f;

	private GameObject character;

	// Use this for initialization
	void Start()
	{
		//sets the character to the cameras parent object
		character = this.transform.parent.gameObject;
        //Gets the initial spawn location from the game manager
        mouseLook.x = GameObject.Find("GameManager").GetComponent<GameManager>().spawnRot.y;
	}

	// Update is called once per frame
	void Update()
	{
		//md = mouse delta
		var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

		//Uses the values of the mouse movement to create lerps to smoothly rotate the player
		md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
		smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
		smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
		mouseLook += smoothV;

		//Locks the cameras y rotation between the two 90 degree angles to stop it from freaking out
		mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);

		//applies the rotations to the camera and player
		transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
		character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
	}
}
