﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RecordableObjectBase : MonoBehaviour
{
    public float timeStamp = 0;
    public List<Keyframe> Keyframes = new List<Keyframe>();

    public delegate void Replaying();
    public static event Replaying FinishReplay;

    public void Record(Keyframe newKeyframe)
    {
        //Creates a new keyframe with current timestamp
        //Create a timestamp timer within your inherited class
        newKeyframe.timestamp = timeStamp;
        //Adds new keyframe as a specific keyframe
        if (newKeyframe is Keyframe)
        {
            Keyframes.Add(newKeyframe as Keyframe);
        }
        //For your own custom keyframe types, create an else if following the same format as above
    }

    public IEnumerator Replay()
    {
        foreach (Keyframe key in Keyframes)
        {
            //Spaces out each replayed key using their timestamp and game time
            //Time.deltaTime should be replaced by a variable representing the time since the start of the replay
            yield return new WaitForSeconds(key.timestamp - Time.deltaTime);
            //Replays the current key as its keyframe type
            if (key is Keyframe)
            {

            }
        }
        if (FinishReplay != null) FinishReplay();
    }
}

[System.Serializable]
public class Keyframe
{
    public float timestamp;
}
