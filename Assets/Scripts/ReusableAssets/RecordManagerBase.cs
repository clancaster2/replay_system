﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordManagerBase : MonoBehaviour
{
    public List<GameObject> RecordedObjects = new List<GameObject>();
    public int objFinishedReplay;

    public bool isReplaying;

    public delegate void Replaying();
    public static event Replaying ReplayStarted;
    public static event Replaying ReplayStopped;
    public static event Replaying ReplayFinished;

    private List<Coroutine> replayList = new List<Coroutine>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddRecordableObject(GameObject objToAdd)
    {
        //Adds given object to list of all objects to record and replay
        RecordedObjects.Add(objToAdd);
    }

    public void IncrementObjFinished()
    {
        //Keeps a count of the amount of objects whose replays have completed, 
        //signalling full replay completion once the number of object finished is equal to the number of objects recorded
        objFinishedReplay++;
        if (objFinishedReplay == RecordedObjects.Count)
        {
            if (ReplayFinished != null) ReplayFinished();
        }
    }

    public void PlayRecording()
    {
        //Event call announcing that a replay has begun
        isReplaying = true;
        if (ReplayStarted != null) ReplayStarted();
        
        //Activates replay function for each recorded object
        foreach (GameObject item in RecordedObjects)
        {
            //Stores each objects replay coroutine within a list
            replayList.Add(StartCoroutine(item.GetComponent<RecordableObjectBase>().Replay()));
        }
    }

    //Event call announcing a replay is being stopped before completion
    public void StopRecording()
    {
        //stops each reply couroutine, cannot be resumed(?)
        foreach(Coroutine replayCoroutine in replayList)
        {
            StopCoroutine(replayCoroutine);
        }
        if (ReplayStopped != null) ReplayStopped();
    }
}
